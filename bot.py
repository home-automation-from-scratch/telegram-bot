from flask import Flask, request
import threading
import logging
import requests
from requests.exceptions import HTTPError
import json
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


#---------CONFIGURATION-------------
token = "893253165:AAGo2aKtvzFOU1KgZGakUvsksYFTQh3F6g8"
eventManagerURL = "http://127.0.0.1:5000"
#-----------------------------------

flask = Flask(__name__)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Create the Updater and pass it your bot's token.
# Make sure to set use_context=True to use the new context based callbacks
# Post version 12 this will no longer be necessary
updater = Updater(token, use_context=True)

# Get the dispatcher to register handlers
# dispatcher has the bot object, responsible for sending messages
dp = updater.dispatcher

# Log Errors caused by Updates (from example code)
def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)

# Send back every message received
def echo(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text=update.message.text)

# Send a message with the commands available to the user
def help(update, context):
    text = "Hello, this is my Bot \n"
    text += "The only commands supported at this moment are"
    text += "/help - which you just used"
    text += "/test - prints the message given and Chat ID to console"
    text += "/set - arguments 'location' 'actuator' 'value' activates an actuator"
    context.bot.send_message(chat_id=update.message.chat_id, text=update.message.text) # answer to the sender



# Print the received message and id to console
def test(update, context):
    id=update.message.chat_id # chat id, remains the same for each user
    message=update.message.text # incoming message
    print(id, message)
    context.bot.send_message(chat_id=id, text=str(id)+"  "+message) # reply with id and message



# Command the Manager to change the state of an actuator
def set(update, context):
    id=update.message.chat_id # incomming message chat id
    text=update.message.text # incomming message
    params = text.split() #split string into a list

    # first parameter is the command itself
    # the other three parameters are the ones shown below
    # the if statement ensures that 3 parameters are given.

    if ( len(params) != 4 ): # if the parameter count is wrong
        msg = "Message form \"/set [location] [actuator] [value]\"" # answer to the user with the right command form
        context.bot.send_message(chat_id=id, text=msg)
    else: # if the parameter count is righ
        location = str(params[1])
        actuator = str(params[2])
        value = str(params[3])
        if str.isdigit(value):
            value = int(value)
        command = {"location" : location, # create dict with the parameters used as values to the corresponting keys
                "actuator" : actuator,
                "value" : value
                }

        # post the command to the corresponting manager url
        url = eventManagerURL + "/postCommand"
        postData(url, command)



# Command the Manager to change the state of an actuator
def get(update, context):
    id=update.message.chat_id # incomming message chat id
    text=update.message.text # incomming message
    params = text.split() #split string into a list

    # first parameter is the command itself
    # the other three parameters are the ones shown below
    # the if statement ensures that 2 parameters are given.

    if ( len(params) != 3 ): # if the parameter count is wrong
        msg = "Message form \"/get [location] [sensor] \"" # answer to the user with the right command form
        context.bot.send_message(chat_id=id, text=msg)
    else: # if the parameter count is righ
        location = str(params[1])
        sensor = str(params[2])

    headers = {'Content-type': 'application/json'} # neccessary fot compatibility
    data = {"location":location, "sensor":sensor}
    URL = eventManagerURL + "/retrieveData"
    response = None
    try:
        # the GET request is executed, the server response is stored
        response = requests.get(url=URL, data=json.dumps(data), headers=headers)
         # If the response was successful, no Exception will be raised
        response.raise_for_status()
        code = response.status_code
        print('HTTP Code:', code)

        payload = response.text
        #msg = payload["value"]
        context.bot.send_message(chat_id=id, text=payload)

    except Exception as e:
        print("Exception: ", e)
        context.bot.send_message(chat_id=id, text="Error")


# post a dict as json to a given URL
def postData(URL, data):
    headers = {'Content-type': 'application/json'} # neccessary fot compatibility
    try:
        # the POST is executed the server response is stored
        response = requests.post(url=URL, data=json.dumps(data), headers=headers)
         # If the response was successful, no Exception will be raised
        response.raise_for_status()
        code = response.status_code
        print('HTTP Code:', code)
    except HTTPError as http_err:
        print('Error posting to event manager')

def main():
    # on different commands take the corresponting action
    dp.add_handler(CommandHandler("test", test))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("set", set))
    dp.add_handler(CommandHandler("get", get))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    print('Bot Started')

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    #updater.idle()

# Send the received text to Telegram chat
@flask.route('/notify', methods=['POST'])
def notify():
    data = request.data  # received data
    dp.bot.sendMessage(chat_id=555992195, text=data) # send the text received as messages
    # on later version the chat ids will be stored and retrieved by the DB
    return "This is a test."

# def sigint_handler(signal, frame):
#     print 'Interrupted'
#     sys.exit(0)

if __name__ == '__main__':
    # start telegram bot and flask module as separate threads
    threading.Thread(target=main).start()
    threading.Thread(target=flask.run(host='127.0.0.1', port = 7000)).start()
