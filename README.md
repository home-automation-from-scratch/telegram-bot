# Telegram Bot

Telegram Bot is used to receive instructions in order to change the state of actuators.
It will also be used to notify certain user(s) for urgent changes received by the sensors.

Be sure to configure the script by using your bot's token.
